% logistic_regression.m


% Maximum number of iterations.  Continue until this limit, or when error change is below tol.
max_iter=500;
tol = 0.01;

% Step size for gradient descent.
eta = 0.003;

% Wait for user when drawing plots.
wait_user = false;


% Get X1, X2
load('data.mat');

% Data matrix, with column of ones at end.
X = [X1; X2];
X = [X ones(size(X,1),1)];
% Target values, 0 for class 1 (datapoints X1), 1 for class 2 (datapoints X2).
t = [zeros(size(X1,1),1); ones(size(X2,1),1)];

% Initialize w.
w = [0.1 0 0]';

% Error values over all iterations.
%e_all = [];
% Set up the slope-intercept figure
figure(2);
clf;
set(gca,'FontSize',15);
title('Separator in slope-intercept space');
xlabel('slope');
ylabel('intercept');
axis([-5 5 -10 0]);
axis equal;
axis manual;

e_all = [zeros(1,max_iter)];
 s = randperm(200,200);
for iter=1:max_iter
    for j = 1:200
  % Compute output using current w on all data X.
  y = sigmoid(w'*X(s(j),:)')';
  
  % e is the error, negative log-likelihood (Eqn 4.90)
  e =  -(t(s(j),:).*log(y) + (1-t(s(j),:)).*log(1-y));

  % Add this error to the end of error vector.
  e_all(iter) = e_all(iter) + e;

  % Gradient of the error, using Eqn 4.91
  grad_e = sum(repmat(y - t(s(j),:),[1 size(X(s(j),:),2)]) .* X(s(j),:), 1);
 % grad_e = y-t(s,:) .* X(s,:);
  % Update w, *subtracting* a step in the error derivative since we're minimizing
  w_old = w;
  w = w - eta*grad_e';
    end;
   
  % Plot current separator and data.
  figure(1);
  set(gca,'FontSize',15);
  plot(X1(:,1),X1(:,2),'g.');
  hold on;
  plot(X2(:,1),X2(:,2),'b.');
  drawSep(w);
  hold off;
  title('Separator in data space');
  axis([-5 15 -10 10]);
  axis equal;
  axis manual;

  
  % Add next step of separator in m-b space.
  figure(2);
  hold on;
  plotMB(w,w_old);
  hold off;
  
  % Print some information.
  fprintf('iter %d, negative log-likelihood %.4f, w=', iter, e);
  fprintf('%.2f ',w);
  if wait_user
    % Wait for user input.
    input('Press enter');
  else
    fprintf('\n');
  end
  
  
  % Stop iterating if error doesn't change more than tol.
  if iter>1
    if abs(e_all(iter)-e_all(iter-1))<tol
      break;
    end
  end
end

% Plot error over iterations
figure(3)
set(gca,'FontSize',15);
plot(e_all,'b-');
xlabel('Iteration');
ylabel('neg. log likelihood')
title('Minimization using gradient descent');


