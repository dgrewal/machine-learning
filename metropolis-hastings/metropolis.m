%Number of samples to draw.
numSamples = 2000;
%parameters for the distribution from which we will draw the samples.
muP = 4;
sigmaP = 3;
muQ = 0;
sigmaQ = 8;
%p is the distribution from which we want to draw the samples.
%using @ to return handle to the 'samples' in order to make it work for the
%continous values.
p = @(samples) normpdf(samples,muP,sigmaP);

%pevSample to hold the value of the last sample.
prevSample = 0;
% Burn In the loop for 300 runs
for i = 1:301
    possibleSample = prevSample + sigmaQ*randn(1);
    %using Formula for A
    A = min([ 1,( p(possibleSample)/p(prevSample))]);
    %keep the samples with the probability of A
    if(A>rand(1))
        prevSample = possibleSample;
    end;
end;
samplesCollected = 1;
finalSamples = 0;
% now lets loop until we get 2000 samples
while samplesCollected<2000
    %get a sample from the proposal distribution
    possibleSample = finalSamples(samplesCollected) + sigmaQ*randn(1);
    %possibleSample = prevSample + samples(samplesCollected);
    %get A
    A = min([ 1, p(possibleSample)/p(prevSample)]);
    %keep samples with prob of A
    if(A>rand(1))
        %Increment the samples that have been collected.
        samplesCollected = samplesCollected + 1;
        finalSamples(samplesCollected) = possibleSample;
    end;
    
end;
figure, hist(finalSamples,-40:40);
title('MCMC for Normal distribution', 'FontSize', 12)
xlabel('Samples', 'FontSize', 12)


% % %======================================Part2====================%
%pevSample to hold the value of the last sample.
prevSample = 1;
% Burn In the loop for 300 runs.
for i = 0:300
    possibleSample = prevSample + 8*randn(1);
    %using Formula for A
    A = min([ 1,( complex_dist(possibleSample)/complex_dist(prevSample))]);
    %keep the samples with the probability of A
    if(A>rand(1))
        prevSample = possibleSample;
    end;
end;
samplesCollected = 1;
% now lets loop until we get 2000 samples
while samplesCollected<2000
    %get a sample from the proposal distribution
    possibleSample = finalSamples(samplesCollected) + sigmaQ*randn(1);
    %get A
    A = min([ 1, complex_dist(possibleSample)/complex_dist(prevSample)]);
    %keep samples with prob of A
    if(A>rand(1))
        samplesCollected = samplesCollected + 1;
        finalSamples(samplesCollected) = possibleSample;
    end;
    
end;
figure, hist(finalSamples,-40:40);
title('MCMC for Complex distribution', 'FontSize', 12)
xlabel('Samples', 'FontSize', 12)