%Number of samples to draw.
numSamples = 2000;
%parameters for the proposal distribution
muQ = 0;
sigmaQ = 8;
%parameters for the distribution from which we will draw the samples.
muP = 4;
sigmaP = 3;
%Draw samples from normal distribution
samples = sigmaQ*randn(numSamples,1);
samples = sort(samples);

%GEt the distributions
q = normpdf(samples,muQ,sigmaQ);  %proposal distribution
p = normpdf(samples,muP,sigmaP);    %We draw samples from this.
%using formula for the weights we get w
w = (p./q)./(sum(p./q));
%get the cumulative cdf
cumulativeW = cumsum(w);
%generating new samples from an uniform distribution
newSamples = rand(numSamples,1);
%get the indices
[~,indices] = histc(newSamples,cumulativeW);
%get the samples
finalSamples = samples(indices);

%get the plots
figure, hist(finalSamples,-40:40)
title('Sampling Importance Resampling for a Normal Distribution', 'FontSize', 12)
xlabel('Samples', 'FontSize', 14)



%===============================Part 2===================================%


%using formula for the weights we get w
w = (complex_dist(samples)./q)./sum(complex_dist(samples)./q);
%get the cumulative cdf
cumulativeW = cumsum(w);
%generating new samples from an uniform distribution
newSamples = rand(numSamples,1);
%get the indices
[~,indices] = histc(newSamples,cumulativeW);
%get the samples
finalSamples = samples(indices);

%get the plots
figure, hist(finalSamples,-40:40)
title('Sampling Importance Resampling for a Complex Distribution', 'FontSize', 12)
xlabel('Samples', 'FontSize', 14)





