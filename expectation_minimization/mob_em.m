% Code for fitting a mixture of Bernoulli distributions using EM.

% Load the data X
load('digits.mat');

% X is 28x28x1000
% Each X(:,:,i) is a 28x28 image.
% Code to visualize an image is below.
figure(1);
imagesc(X(:,:,2));
colormap gray;
axis image;

% Make data binary by thresholding above 0.5.
X = double(X > 0.5);
% Subsample images to be 14x14, Bernoulli with 28x28=784 numbers gives underflow.
X = X(1:2:end,1:2:end,:);

% 14,14,1000.
[nx,ny,ndata] = size(X);

% Number of iterations of EM
niter=20;

% Number of mixture components
K = 20;

% Responsibilities
resp = zeros(ndata,K);

% Mixing coefficients
Pi = zeros(1,K);

% Bernoulli distribution parameters
% Mu(:,:,k) is the parameters for mixture component k
% Mu(i,j,k) is the probability that pixel (i,j) is 1 in mixture component k
Mu = zeros(nx,ny,K);

% Initialize randomly.
% Mu drawn uniformly from values in [0.25,0.75]
Mu = 0.5*rand(nx,ny,K) + 0.25;

% Pi initialized to make each mixture component have equal weight.
Pi = ones(1,K)/K;

visualizeMu(Mu,2,'Initialization');
input('Initialization, press return');

for iter=1:niter
    % E-step: set responsibilities given current parameters
    for dataIter = 1:ndata
        for kIter = 1:K
            resp(dataIter,kIter) = Pi(kIter)*bernoulli2(X(:,:,dataIter), Mu(:,:,kIter));
        end
        resp(dataIter,1:end) = resp(dataIter,1:end)/sum(resp(dataIter,1:end));
    end

    % M-step: update parameters given responsibilities
    Nk = sum(resp);
    Pi = Nk/ndata;
    for iter2 = 1:K
        reshape_mat = reshape(resp(:,iter2),[1 1 ndata]);
        Mu(:,:,iter2) = sum(bsxfun(@times, reshape_mat, X), 3);
    end
    Mu = bsxfun(@times, reshape(1./Nk, [1 1 K]), Mu);


    visualizeMu(Mu,2,(sprintf('Iteration %d',iter)));
    input(sprintf('Iteration %d, press return',iter));
end

