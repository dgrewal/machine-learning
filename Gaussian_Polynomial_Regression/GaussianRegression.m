function gaussianValErr =  GaussianRegression(lambda,trainingSet_x,validationSet_X,trainingSet_t,validationSet_t)
    
    numbasis = 90;
    s =2;
    
    training_set = trainingSet_x(randperm(90, numbasis),:);
    training_phi = ones(90,1);
    training_mu = dist2(trainingSet_x,training_set);
    training_phi = [training_phi exp(-training_mu/(2*s.^2))];

    validation_phi = ones(10,1);
    validation_mu = dist2(validationSet_X,training_set); 
    validation_phi = [validation_phi exp(-validation_mu/(2*s.^2))];
   
    if lambda == 0
         w = pinv(training_phi)*trainingSet_t;
    else
        brack = (training_phi'*training_phi)+(lambda*eye(numbasis+1));
    w = (inv(brack)) * training_phi'*trainingSet_t;
    end
    gaussianValErr = sqrt(sum(((validation_phi * w) - validationSet_t).^2)/10);
    