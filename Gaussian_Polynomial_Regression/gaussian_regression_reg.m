[t,X]=loadData();
X_n = normalizeData(X);
t = normalizeData(t);

lambda = 0;
totalErr = 0;
%fold 1
validationSet_x = X_n(1:10,:);
validationSet_t = t(1:10,:);
trainingSet_x = X_n(11:100,:);
trainingSet_t = t(11:100,:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 2
validationSet_x = X_n(11:20,:);
validationSet_t = t(11:20,:);
trainingSet_x = X_n([1:10 21:100],:);
trainingSet_t = t([1:10 21:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 3
validationSet_x = X_n(21:30,:);
validationSet_t = t(21:30,:);
trainingSet_x = X_n([1:20 31:100],:);
trainingSet_t = t([1:20 31:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 4
validationSet_x = X_n(31:40,:);
validationSet_t = t(31:40,:);
trainingSet_x = X_n([1:30 41:100],:);
trainingSet_t = t([1:30 41:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 5
validationSet_x = X_n(41:50,:);
validationSet_t = t(41:50,:);
trainingSet_x = X_n([1:40 51:100],:);
trainingSet_t = t([1:40 51:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 6
validationSet_x = X_n(51:60,:);
validationSet_t = t(51:60,:);
trainingSet_x = X_n([1:50 61:100],:);
trainingSet_t = t([1:50 61:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 7
validationSet_x = X_n(61:70,:);
validationSet_t = t(61:70,:);
trainingSet_x = X_n([1:60 71:100],:);
trainingSet_t = t([1:60 71:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 8
validationSet_x = X_n(71:80,:);
validationSet_t = t(71:80,:);
trainingSet_x = X_n([1:70 81:100],:);
trainingSet_t = t([1:70 81:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 9
validationSet_x = X_n(81:90,:);
validationSet_t = t(81:90,:);
trainingSet_x = X_n([1:80 91:100],:);
trainingSet_t = t([1:80 91:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 10
validationSet_x = X_n(91:100,:);
validationSet_t = t(91:100,:);
trainingSet_x = X_n(1:90,:);
trainingSet_t = t(1:90,:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

totalErr = totalErr/10;
error = [];
error = [error totalErr];
totalErr = 0;

lambda = 0.01;
while lambda<= 1000 
 totalErr = 0;
%fold 1
validationSet_x = X_n(1:10,:);
validationSet_t = t(1:10,:);
trainingSet_x = X_n(11:100,:);
trainingSet_t = t(11:100,:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 2
validationSet_x = X_n(11:20,:);
validationSet_t = t(11:20,:);
trainingSet_x = X_n([1:10 21:100],:);
trainingSet_t = t([1:10 21:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 3
validationSet_x = X_n(21:30,:);
validationSet_t = t(21:30,:);
trainingSet_x = X_n([1:20 31:100],:);
trainingSet_t = t([1:20 31:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 4
validationSet_x = X_n(31:40,:);
validationSet_t = t(31:40,:);
trainingSet_x = X_n([1:30 41:100],:);
trainingSet_t = t([1:30 41:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 5
validationSet_x = X_n(41:50,:);
validationSet_t = t(41:50,:);
trainingSet_x = X_n([1:40 51:100],:);
trainingSet_t = t([1:40 51:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 6
validationSet_x = X_n(51:60,:);
validationSet_t = t(51:60,:);
trainingSet_x = X_n([1:50 61:100],:);
trainingSet_t = t([1:50 61:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 7
validationSet_x = X_n(61:70,:);
validationSet_t = t(61:70,:);
trainingSet_x = X_n([1:60 71:100],:);
trainingSet_t = t([1:60 71:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 8
validationSet_x = X_n(71:80,:);
validationSet_t = t(71:80,:);
trainingSet_x = X_n([1:70 81:100],:);
trainingSet_t = t([1:70 81:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 9
validationSet_x = X_n(81:90,:);
validationSet_t = t(81:90,:);
trainingSet_x = X_n([1:80 91:100],:);
trainingSet_t = t([1:80 91:100],:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

%fold 10
validationSet_x = X_n(91:100,:);
validationSet_t = t(91:100,:);
trainingSet_x = X_n(1:90,:);
trainingSet_t = t(1:90,:);
totalErr = totalErr + GaussianRegression(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t);

lambda = lambda * 10;
totalErr = totalErr/10;
error = [error totalErr];
totalErr = 0;
end
lambda = [0 0.01 0.1 1 10 100 1000];
semilogx(lambda, error);
set(gca,'FontSize',15);
title('Gaussian Regression with Regularization');
xlabel('Lambda');
ylabel('Error');