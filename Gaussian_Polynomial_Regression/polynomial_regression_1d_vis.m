[t,X]=loadData();
X_n = normalizeData(X);
t = normalizeData(t);

%Training Set
trainingSet_X = X_n(1:100,3);
trainingSet_t = t(1:100,:);

%Testing Set
x_ev = (min(trainingSet_X) : 0.1:max(trainingSet_X))';
testSet_t = t(101:392,:);
testSet_X = X_n(101:392,3);
%preinitialize the matrices to store the result

%the outermost loop from 1 to 10.. Filts all the 10 cases in one row and
%saves the error results in the matrices
  for n = 1:10

    for i = 1:n
        designMatrix = ones(100,1);
        for j = 1:i
         designMatrix = [designMatrix trainingSet_X.^j];
        end
    end
    
    for i = 1:n
        designMatrix_test = ones(47,1);
        for j = 1:i
         designMatrix_test = [designMatrix_test x_ev.^j];
        end
    end
    
    w = pinv(designMatrix)*trainingSet_t;
    y_ev = designMatrix_test * w;

    figure;
% Make the fonts larger, good for reports.
set(gca,'FontSize',15);
plot(x_ev,y_ev,'r.-');  
hold on;
plot(trainingSet_X,trainingSet_t,'g.');
plot(testSet_X,testSet_t,'bo');
hold off;
title(sprintf('Fit with degree %d polynomial',n));

  end

