figure;
% Make the fonts larger, good for reports.
set(gca,'FontSize',15);

plot(y, 'r');
hold on;
plot(trainingSet_X,trainingSet_t,'g.');
hold on;
plot(testSet_X,testSet_t,'bo');
