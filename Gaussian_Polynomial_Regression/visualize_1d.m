% X_n is 1-d
% X_train, X_test, t_train, t_test should all be 1-d, and need to be defined as well.
% You should modify y_ev

% Plot a curve showing learned function.
%x_ev = trainingSet_X;
x_ev = (min(trainingSet_X) : 0.1:max(trainingSet_X))';
% Put your regression estimate here.
y_ev = designMatrix * w;

figure;
% Make the fonts larger, good for reports.
set(gca,'FontSize',15);
plot(x_ev,y_ev,'r.-');  
hold on;
plot(trainingSet_X,trainingSet_t,'g.');
plot(testSet_X,testSet_t,'bo');
hold off;
title(sprintf('Fit with degree %d polynomial',5));
