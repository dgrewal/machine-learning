function validationError =  crossValidation(lambda,trainingSet_x,validationSet_x,trainingSet_t,validationSet_t)
n = 8;
 for i = 1:n
        designMatrix_training = ones(90,1);
        for j = 1:i
         designMatrix_training = [designMatrix_training trainingSet_x.^j];
        end
 end
    
    for i = 1:n
        designMatrix_validation = ones(10,1);
        for j = 1:i
            designMatrix_validation = [designMatrix_validation validationSet_x.^j];
        end
    end
    
    w = inv((designMatrix_training' * designMatrix_training) + (lambda* eye(n+1)))* designMatrix_training' * trainingSet_t;
    
    validationError = sqrt(sum(((designMatrix_validation * w) - validationSet_t).^2)/10);
  
   