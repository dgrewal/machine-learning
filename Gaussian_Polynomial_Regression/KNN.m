function nwError = KNN(h,trainingSet_X,validationSet_X,trainingSet_t,validationSet_t)
%training
expectation = [];
numerator = [];
denominator = [];
for i = 1:10
    for j = 1:90
    numerator = [numerator, trainingSet_t(j) * calculateG( validationSet_X(i) - trainingSet_X(j), h )];
    denominator = [denominator, calculateG(validationSet_X(i) - trainingSet_X(j), h )];
    end;
    expectation =[expectation, (sum(numerator)/(sum(denominator)+eps()))];

end;

error = [];
%for k = 1:10
   error = (expectation' - validationSet_t);
%end

nwError = sqrt( (sum(error.^2)) / 10);