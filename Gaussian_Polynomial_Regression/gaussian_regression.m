[t,X]=loadData();
X_n = normalizeData(X);
t = normalizeData(t);

%Training Set
trainingSet_X = X_n(1:100,:);
trainingSet_t = t(1:100,:);

%Testing Set
testSet_X = X_n(101:392,:);
testSet_t = t(101:392,:);

%preinitialize the matrices to store the result
trainingError = [];
testError = [];
s = 2;
%design Matrices

numbasis  =5 ;
numbasis_matrix = [5,15,25,35,45,55,65,75,85,95];

while numbasis <=95
    training_phi = ones(100,1);
    training_set = trainingSet_X(randperm(100, numbasis),:);
    training_mu = dist2(trainingSet_X,training_set);
    training_phi = [training_phi exp(-training_mu/(2*s.^2))];

    testing_phi = ones(292,1);
    testing_mu = dist2(testSet_X,training_set); 
    testing_phi = [testing_phi exp(-testing_mu/(2*s.^2))];

    w = pinv(training_phi)*trainingSet_t;

    trainingError = [trainingError sqrt( sum(((training_phi * w) - trainingSet_t).^2) / 100)];

    testError = [testError sqrt( sum(((testing_phi * w) - testSet_t).^2)/292)];

    numbasis = numbasis+10;
end

plot(numbasis_matrix, trainingError,'g');
hold on;
plot(numbasis_matrix,testError, 'r');
hold off;
set(gca,'FontSize',15);
title('Gaussian Regression');
xlabel('Number of Basis Functions');
ylabel('Error');