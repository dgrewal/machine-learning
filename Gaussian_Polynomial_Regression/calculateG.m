function gOfU =  calculateG(u,h)

    if(abs(u)/h <=1)
    gOfU = (3/4)*(1 - ((u.^2)/(h.^2)));
    else
        gOfU =0;
    end
end