%% Perceptron Algorithm Implementation %%
clear;

 % Load the 2 classes of data  One is in X1 other in X2
    load('abalone.mat');
    %now we need to prime the testdata. set class with classlabel = classes
    %to +1 and rest of all to -1.
    
    for arr = 1:size(trainingTargets)
    if(trainingTargets(arr) ~= 1)
        trainingTargets(arr) = -1;
    end;
    end;
for arr = 1:size(testTargets)
    if(testTargets(arr) ~= 1)
        testTargets(arr) = -1;
    end;
    end;
% The maximum number of iterations
maxIter=500;
%Error Tolerance
errTol = 0.1;
%Step Size eta for batch/stochastic G.D.
eta = 10;

%Wait for user when drawing plots
waitUser = false;



%The number of classes
numClasses = 3;
%weights for each of the classes
weightClass= [];
%Merge into one matrix with a column of ones at the end
%already merged for ionosphere data
%X = [X1;X2];


%get the target values for the classes
%t= [-ones(size(X1,1),1); ones(size(X2,1),1)];
%already loaded into the trainingTargets matrix 

%initialize W to some random value
w = [0.1 ones(1,8)]';

% Set up the slope-intercept figure
figure(2);
clf;
set(gca,'FontSize',15);
title('Separator in slope-intercept space');
xlabel('slope');
ylabel('intercept');
axis([-5 5 -10 0]);
axis equal;
axis manual;

%an array to hold the errors for a given run through the data
e_all = [zeros(1,maxIter)];
% The randperm generates 200 random numbers between 1 and 200 ; we use it
% to choose the data points from the dataset randomly
 s = randperm(200,200);
 
%%Iterate through the data maxIter times; for each iteration in maxIter we
%%run through the data 200 times.

   
    
    trainingData = [trainingData ones(size(trainingData,1),1)];
    %now we have a binary problem,Run percpetron
for iter=1:maxIter
    for j = 1:200
        % Compute output using current w on random datapoint X(s(j),:).
        % perceptron_y is the implementation on the f function returns {+1,-1}
         y = perceptron_y(w'*trainingData(s(j),:)');
  
        %if the product of y and the target output is negative then we need to
        %update the weight vector.
        if (y*trainingTargets(s(j),:)<0)
            % e is the error
            e = -((w'*trainingData(s(j),:)')*trainingTargets(s(j),:));
            % Add this error to the current running sum of the error.
            e_all(iter) = e_all(iter) + e;
            % Gradient of the error
            grad_e =  trainingData(s(j),:).*trainingTargets(s(j),:);
            % Update w, *subtracting* a step in the error derivative since we're minimizing
            w_old = w;
            w = w + eta*grad_e';
        end;
    end;

    
    
% Plot current separator and data.
  figure(1);
  set(gca,'FontSize',15);
  plot(trainingData(:,1),trainingData(:,2),'g.');
  hold on;
  %plot(X2(:,1),X2(:,2),'b.');
  drawSep(w);
  hold off;
  title('Separator in data space');
  axis([-5 15 -10 10]);
  axis equal;
  axis manual;
  
% Add next step of separator in m-b space.
  figure(2);
  hold on;
  plotMB(w,w_old);
  hold off;
  
  % Print some information.
  fprintf('iter %d, negative log-likelihood %.4f, w=', iter, e);
  fprintf('%.2f ',w);
  if waitUser
    % Wait for user input.
    input('Press enter');
  else
    fprintf('\n');
  end
  
  
  % Stop iterating if error doesn't change more than tol.
  if iter>1
    if abs(e_all(iter)-e_all(iter-1))<errTol
      break;
    end
  end
end

% Plot error over iterations
figure(3)
set(gca,'FontSize',15);
plot(e_all,'b-');
xlabel('Iteration');
ylabel('neg. log likelihood')
title('Minimization using gradient descent');

