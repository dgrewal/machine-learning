% %% Now we Have all the required weight vectors stored in the array w%% 
% %%(access them using w(:,j)) and all the counts are saved in the array c%%
% 
% %%Lets start predicting now
% %%creating a datapoint
% totalVote = 0;
% answ = [];
% predictions = [];
% testData = [X1 ones(size(X1,1),1)];
% for j = 1:100
%     answ = [];
%     for i = 1: n
%         answ = [answ (c(i) * sign(w' * testData(j,:)'))];
%     end;
%  predictions = [predictions sign(sum(answ))];
% end;
% sum(predictions)

%% Now we Have all the required weight vectors stored in the array w%% 
%%(access them using w(:,j)) and all the counts are saved in the array c%%

%%Lets start predicting now
%%creating a datapoint
totalVote = 0;
result = [];
testData = [testData ones(size(testData,1),1)];
j = 0;
i = 0;
%we already have test data
%testData = [3.81561160773515,-4.27550268683578,1];
for j = 1:size(testData,1)
    for i = 1: size(c,2)
        totalVote = totalVote + (c(i) * sign(w(:,i)' * testData(j,:)'));
    end;
result = [result sign(totalVote)];
totalVote = 0;
end;

j = 0;
i = 0;
for i = 1:151
    if(result(i)~= testTargets(i))
        j = j+1;
    end;
end;