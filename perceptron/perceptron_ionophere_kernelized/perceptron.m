%% Perceptron Algorithm Implementation %%
clear;
% The maximum number of iterations
maxIter=500;
%Error Tolerance
errTol = 0.01;
%Step Size eta for batch/stochastic G.D.
eta = 10;

%Wait for user when drawing plots
waitUser = false;

load('ionosphere.mat');


%initialize W to some random value
w = [0.1 ones(1,351)]';

%an array to hold the errors for a given run through the data
e_all = [zeros(1,maxIter)];

% generate random numbers between 1 and 200; to choose points from the dataset
 s = randperm(200,200);
 
 
 % Generate the Kernel
 for i=1:size(ionosphere,1);
    for j=1:size(ionosphere,1);
        K(i,j)=exp((-norm(ionosphere(i,1:34)'-ionosphere(j,1:34)').^2)./2);
    end
 end
%split the k matrix into the training and test data
 trainingData = []; trainingTargets = [];
 testData = []; testTargets = [];
 
 trainingData = K(1:200,:);
 trainingTargets = ionosphere(1:200,35);
 testData = K(201:end,:);
 testTargets = ionosphere(201:end,35);
 
 
 trainingData = [trainingData ones(size(trainingData,1),1)];
 
%%Iterate through the data maxIter times; for each iteration in maxIter we
%%run through the data 200 times.
for iter=1:maxIter
    for j = 1:200
        % Compute output using current w on random datapoint X(s(j),:).
        % perceptron_y is the implementation on the f function returns {+1,-1}
         y = perceptron_y(w'*trainingData(s(j),:)');
  
        %if the product of y and the target output is negative then we need to
        %update the weight vector.
        if (y*trainingTargets(s(j),:)<0)
            % e is the error
            e = -((w'*trainingData(s(j),:)')*trainingTargets(s(j),:));
            % Add this error to the current running sum of the error.
            e_all(iter) = e_all(iter) + e;
            % Gradient of the error
            grad_e =  trainingData(s(j),:).*trainingTargets(s(j),:);
            % Update w, *subtracting* a step in the error derivative since we're minimizing
            w_old = w;
            w = w + eta*grad_e';
        end;
    end;

  
  % Print some information.
  fprintf('iter %d, negative log-likelihood %.4f, w=', iter, e);
  fprintf('%.2f ',w);
  if waitUser
    % Wait for user input.
    input('Press enter');
  else
    fprintf('\n');
  end
  
  
  % Stop iterating if error doesn't change more than tol.
%   if iter>1
%     if abs(e_all(iter)-e_all(iter-1))<errTol
%       break;
%     end
%   end
end

% Plot error over iterations
figure(3)
set(gca,'FontSize',15);
plot(e_all,'b-');
xlabel('Iteration');
ylabel('neg. log likelihood')
title('Minimization using gradient descent');

