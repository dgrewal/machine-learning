%% Perceptron Algorithm Implementation %%

% The maximum number of iterations
maxIter=500;
%Error Tolerance
errTol = 0.01;
%Step Size eta for batch/stochastic G.D.
eta = 10;

%Wait for user when drawing plots
waitUser = false;

% Load the 2 classes of data  One is in X1 other in X2
load('data.mat');

%Merge into one matrix with a column of ones at the end
X = [X1;X2];
X = [X ones(size(X,1),1)];

%get the target values for the classes
t= [-ones(size(X1,1),1); ones(size(X2,1),1)];

%initialize W to some random value
w = [0.1 0 0]';

% Set up the slope-intercept figure
figure(2);
clf;
set(gca,'FontSize',15);
title('Separator in slope-intercept space');
xlabel('slope');
ylabel('intercept');
axis([-5 5 -10 0]);
axis equal;
axis manual;

%an array to hold the errors for a given run through the data
e_all = [zeros(1,maxIter)];
% The randperm generates 200 random numbers between 1 and 200 ; we use it
% to choose the data points from the dataset randomly
 s = randperm(200,200);
 
%%Iterate through the data maxIter times; for each iteration in maxIter we
%%run through the data 200 times.
for iter=1:maxIter
    for j = 1:200
        % Compute output using current w on random datapoint X(s(j),:).
        % perceptron_y is the implementation on the f function returns {+1,-1}
         y = perceptron_y(w'*X(s(j),:)');
  
        %if the product of y and the target output is negative then we need to
        %update the weight vector.
        if (y*t(s(j),:)<0)
            % e is the error
            e = -((w'*X(s(j),:)')*t(s(j),:));
            % Add this error to the current running sum of the error.
            e_all(iter) = e_all(iter) + e;
            % Gradient of the error
            grad_e =  X(s(j),:).*t(s(j),:);
            % Update w, *subtracting* a step in the error derivative since we're minimizing
            w_old = w;
            w = w + eta*grad_e';
        end;
    end;
    
% Plot current separator and data.
  figure(1);
  set(gca,'FontSize',15);
  plot(X1(:,1),X1(:,2),'g.');
  hold on;
  plot(X2(:,1),X2(:,2),'b.');
  drawSep(w);
  hold off;
  title('Separator in data space');
  axis([-5 15 -10 10]);
  axis equal;
  axis manual;
  
% Add next step of separator in m-b space.
  figure(2);
  hold on;
  plotMB(w,w_old);
  hold off;
  
  % Print some information.
  fprintf('iter %d, negative log-likelihood %.4f, w=', iter, e);
  fprintf('%.2f ',w);
  if waitUser
    % Wait for user input.
    input('Press enter');
  else
    fprintf('\n');
  end
  
  
  % Stop iterating if error doesn't change more than tol.
  if iter>1
    if abs(e_all(iter)-e_all(iter-1))<errTol
      break;
    end
  end
end

% Plot error over iterations
figure(3)
set(gca,'FontSize',15);
plot(e_all,'b-');
xlabel('Iteration');
ylabel('neg. log likelihood')
title('Minimization using gradient descent');

