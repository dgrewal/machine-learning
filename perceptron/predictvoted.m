% %% Now we Have all the required weight vectors stored in the array w%% 
% %%(access them using w(:,j)) and all the counts are saved in the array c%%
% 
% %%Lets start predicting now
% %%creating a datapoint
% totalVote = 0;
% answ = [];
% predictions = [];
% testData = [X1 ones(size(X1,1),1)];
% for j = 1:100
%     answ = [];
%     for i = 1: n
%         answ = [answ (c(i) * sign(w' * testData(j,:)'))];
%     end;
%  predictions = [predictions sign(sum(answ))];
% end;
% sum(predictions)

%% Now we Have all the required weight vectors stored in the array w%% 
%%(access them using w(:,j)) and all the counts are saved in the array c%%

%%Lets start predicting now
%%creating a datapoint
totalVote = 0;
finalResult = 0;

testData = [3.81561160773515,-4.27550268683578,1];
for i = 1: n
   totalVote = totalVote + (c(i) * sign(w(:,i)' * testData'));
end;

finalResult = sign(totalVote)