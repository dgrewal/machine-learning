%% Perceptron Algorithm Implementation %%
clear;

% Load the 2 classes of data  One is in X1 other in X2
load('wdbc.mat');

% The maximum number of iterations
maxIter=500;
%Error Tolerance
errTol = 0.1;
%Step Size eta for batch/stochastic G.D.
eta = 5;

%Wait for user when drawing plots
waitUser = false;



%Merge into one matrix with a column of ones at the end
%already merged for ionosphere data
%X = [X1;X2];
trainingData = [trainingData ones(size(trainingData,1),1)];

%get the target values for the classes
%t= [-ones(size(X1,1),1); ones(size(X2,1),1)];
%already loaded into the trainingTargets matrix 

%initialize W to some random value
w = [0.1 ones(1,30)]';


%an array to hold the errors for a given run through the data
e_all = [zeros(1,maxIter)];
% The randperm generates 200 random numbers between 1 and 200 ; we use it
% to choose the data points from the dataset randomly
 s = randperm(370,370);
 
%%Iterate through the data maxIter times; for each iteration in maxIter we
%%run through the data 200 times.
for iter=1:maxIter
    for j = 1:370
        % Compute output using current w on random datapoint X(s(j),:).
        % perceptron_y is the implementation on the f function returns {+1,-1}
         y = perceptron_y(w'*trainingData(s(j),:)');
  
        %if the product of y and the target output is negative then we need to
        %update the weight vector.
        if (y*trainingTargets(s(j),:)<0)
            % e is the error
            e = -((w'*trainingData(s(j),:)')*trainingTargets(s(j),:));
            % Add this error to the current running sum of the error.
            e_all(iter) = e_all(iter) + e;
            % Gradient of the error
            grad_e =  trainingData(s(j),:).*trainingTargets(s(j),:);
            % Update w, *subtracting* a step in the error derivative since we're minimizing
            w_old = w;
            w = w + eta*grad_e';
        end;
    end;
    
  
  % Print some information.
  fprintf('iter %d, negative log-likelihood %.4f, w=', iter, e);
  fprintf('%.2f ',w);
  if waitUser
    % Wait for user input.
    input('Press enter');
  else
    fprintf('\n');
  end
  
  
  % Stop iterating if error doesn't change more than tol.
  if iter>1
    if abs(e_all(iter)-e_all(iter-1))<errTol
      break;
    end
  end
end

% Plot error over iterations
figure(3)
set(gca,'FontSize',15);
plot(e_all,'b-');
xlabel('Iteration');
ylabel('neg. log likelihood')
title('Minimization using gradient descent');

