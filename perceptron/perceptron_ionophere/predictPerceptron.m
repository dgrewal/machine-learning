%add ones to the end of the test data matrix
testData = [testData ones(size(testData,1),1)];

result = [];
for i = 1: size(testData,1)
   result = [result  sign(w' * testData(i,:)')];
end;
j = 0;

for i = 1:size(result,2)
    if(result(i)~= testTargets(i))
        j = j+1;
    end;
end;
