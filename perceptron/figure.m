%% Figure Multiclass;

normperc = [57.35 68.80 61.30 68.20 68.10 72.95 62.25 64.50 58.15 69.70];
avgperc  = [72.35 72.95 72.50 73.65 72.85 71.45 73.20 72.60 72.90 72.55];
SVM      = [81.25 81.25 81.25 81.25 81.25 81.25 81.25 81.25 81.25 81.25];


plot(normperc, '-r', 'LineWidth',2);
hold on;
plot(avgperc, '-g', 'LineWidth',2);
plot(SVM, '-k', 'LineWidth',2);
hold off;
set(gca,'FontSize',14);
title('The accuracy of SVM and the perceptrons');
xlabel('number of runs');
ylabel('Accuracy acheived');


%% Figure single class

set(gca,'FontSize',15);
normperc = [89.95 92.96 93.47 92.96 89.45 94.47 63.32 79.90 91.96 87.94 93.47 89.95 88.94 79.90 89.45];
avgperc  = [91.96 89.95 92.46 92.96 90.95 90.45 90.45 93.45 91.46 91.96 91.96 93.47 90.95 93.47 90.95];
votperc  = [92.46 89.45 91.46 93.47 89.95 92.46 90.95 90.95 91.96 93.47 93.97 93.97 91.46 92.46 89.45];
SVM      = [94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6 94.6];
hold on;
plot(normperc, '-r', 'LineWidth',2);

plot(avgperc, '-g', 'LineWidth',2);

plot(votperc, '-b', 'LineWidth',2);

plot(SVM, '-k', 'LineWidth',2);

hold off;
set(gca,'FontSize',14);
title('The accuracy of SVM and the perceptrons in Binary Classification');
xlabel('number of runs');
ylabel('Accuracy acheived');

%% Figure single class

set(gca,'FontSize',15);
normperc = [.2028 .4212 .6396 .8892 1.0920 1.2012 1.4040 1.6536 1.8878 1.9344];
votperc  = [.9984 1.9968 2.6364 3.2136 4.2900 5.2096 6.3492 6.7704 8.1433];
avgperc  = [.1710 .2694 .4212 .6396 .6864 .8112 1.1544 1.17 1.2012 1.3572];
SVM      = [3.98 3.98 3.98 3.98 3.98 3.98 3.98 3.98 3.98 3.98 3.98 3.98];
base     = [50 100 150 200 250 300 350 400 450 500];
plot(base,normperc, '-r', 'LineWidth',2);
hold on;
plot(base,avgperc, '-g', 'LineWidth',2);
hold on;
plot(base,votperc, '-b', 'LineWidth',2);
hold on;
plot(base,SVM, '-k', 'LineWidth',2);

hold on;
set(gca,'FontSize',14);
title('The time taken to train on the binary data');
xlabel('number of runs');
ylabel('Time taken in seconds');
hold off;